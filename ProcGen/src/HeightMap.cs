﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ProcGen
{
    public class HeightMap : IHeightData
    {
        public string HeightMapFilename = "hillz_bmp";
        
        public int TerrainWidth { get; set; }
        public int TerrainHeight { get; set; }
        public float[,] HeightData { get; set; }

        private Color[] _heightMapColors;

        public void Load(ContentManager content)
        {
            var heightmap = content.Load<Texture2D>(HeightMapFilename);
            
            LoadHeightData(heightmap);
        }

        public Color ColorAt(int x, int y)
        {
            return _heightMapColors[x + y * TerrainHeight];
        }

        private void LoadHeightData(Texture2D heightMap)
        {
            TerrainWidth = heightMap.Width;
            TerrainHeight = heightMap.Height;

            _heightMapColors = new Color[TerrainWidth * TerrainHeight];
            heightMap.GetData(_heightMapColors);

            HeightData = new float[TerrainWidth, TerrainHeight];

            for (var x = 0; x < TerrainWidth; x++)
            for (var y = 0; y < TerrainHeight; y++)
                HeightData[x, y] = _heightMapColors[x + y * TerrainWidth].R;
        }
        
        public void ExportAsPng(GraphicsDevice graphics, string fileName)
        {
        }

        public void Move()
        {
        }
    }
}