﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ProcGen
{
    public class DeviceStates
    {
        private bool _drawWireframes;
        private GraphicsDevice _graphicsDevice;
        private RasterizerState _rasterizerStateWireFrame;
        private RasterizerState _rasterizerStateSolid;

        public DeviceStates(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
        }

        public void Initialize(bool wireframe)
        {
            _drawWireframes = wireframe;

            _rasterizerStateWireFrame = new RasterizerState
            {
                FillMode = FillMode.WireFrame
            };

            _rasterizerStateSolid = new RasterizerState
            {
                FillMode = FillMode.Solid
            };

            updateRasterizerStates();
            
            Context.KeyboardListener.KeyPressed += (sender, args) =>
            {
                if (args.Key != Keys.A) return;
                _drawWireframes = !_drawWireframes;
                updateRasterizerStates();
            };
        }

        /// <summary>
        /// Calls to SpriteBatch.Begin change several GraphicsDevice states. So 3D models won't be rendered correctly 
        /// anymore
        /// </summary>
        public void ResetStates()
        {
            _graphicsDevice.BlendState = BlendState.Opaque;
            _graphicsDevice.DepthStencilState = DepthStencilState.Default;
            _graphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            updateRasterizerStates();
        }

        private void updateRasterizerStates()
        {
            _graphicsDevice.RasterizerState = _drawWireframes ? _rasterizerStateWireFrame : _rasterizerStateSolid;
        }
    }
}