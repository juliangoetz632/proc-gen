﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.
    Extended.Input.InputListeners;

namespace ProcGen
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;
        private Camera3D _camera;

        private SpriteFont _spriteFont;

        private Terrain _terrain;
        private DeviceStates _deviceStates;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _graphics.PreferredBackBufferWidth = 1440;
            _graphics.PreferredBackBufferHeight = 900;
            _graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            InitInput();
            InitWorld();
            _camera = new Camera3D(GraphicsDevice);
            _deviceStates = new DeviceStates(GraphicsDevice);
            _deviceStates.Initialize(true);

            base.Initialize();
        }

        private void InitWorld()
        {
            _terrain = new Terrain(GraphicsDevice);
        }

        private void InitInput()
        {
            var keyboardListener = new KeyboardListener(new KeyboardListenerSettings());
            var mouseListener = new MouseListener();
            var inputListenerComponent = new InputListenerComponent(this, keyboardListener, mouseListener);
            Components.Add(inputListenerComponent);
            Context.KeyboardListener = keyboardListener;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _spriteFont = Content.Load<SpriteFont>("font");
            _terrain.LoadContent(Content);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            _camera.Update(gameTime);
            base.Update(gameTime);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            DrawGround();

            DrawDebugInfo(gameTime);

            _deviceStates.ResetStates();

            base.Draw(gameTime);
        }

        private void DrawDebugInfo(GameTime gameTime)
        {
            _spriteBatch.Begin();

            var y = 10;

            var frameRate = 1 / (float) gameTime.ElapsedGameTime.TotalSeconds;
            _spriteBatch.DrawString(_spriteFont, "FPS:" + frameRate, new Vector2(10, y), Color.White);

            foreach (var debugString in Context.DebugStrings)
            {
                _spriteBatch.DrawString(_spriteFont, debugString, new Vector2(10, y += 20), Color.White);
            }

            _spriteBatch.End();
        }

        private void DrawGround()
        {
            var view = _camera.ViewMatrix;
            var projection = _camera.ProjectionMatrix;

            _terrain.Draw(view, projection);
        }
    }

    public class Context
    {
        public static KeyboardListener KeyboardListener;
        public static List<string> DebugStrings = new List<string>();
    }
}