﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ProcGen
{
    public class Terrain
    {
        public bool IsProcedural = true;

        public int TriangleWidth = 1;
        public int TerrainWidth = 500; //number of vertices in x direction
        public int TerrainHeight = 500;

        public IHeightData Heights;

        private VertexPositionColorNormal[] _vertices;
        private int[] _indices;

        private BasicEffect _effect;

        private GraphicsDevice _graphics;
        private float[,] _heightData;

        public Terrain(GraphicsDevice graphicsDevice)
        {
            _graphics = graphicsDevice;
        }

        public void LoadContent(ContentManager content)
        {
            InitLights();

            if (IsProcedural)
            {
                Heights = new ProceduralMap
                {
                    TerrainWidth = TerrainWidth,
                    TerrainHeight = TerrainHeight
                };
                Heights.Load(content);
            }
            else
            {
                Heights = new HeightMap();
                Heights.Load(content);
                TerrainWidth = Heights.TerrainWidth;
                TerrainHeight = Heights.TerrainHeight;
            }

            _heightData = Heights.HeightData;
            
            Heights.ExportAsPng(_graphics, "final.png");

            Context.KeyboardListener.KeyPressed += (sender, args) =>
            {
                if (args.Key == Keys.Z)
                {
                    Heights.Move();
                    InitVertices();
                    AddNormals();
                }
            };

            InitVertices();

            InitIndices();

            AddNormals();
        }

        private void InitLights()
        {
            var direction = Vector3.Left;
            direction.Normalize();
            _effect = new BasicEffect(_graphics)
            {
                //TextureEnabled = false,
                //Texture = _groundTexture,
                VertexColorEnabled = true,
                LightingEnabled = true,
                DirectionalLight0 =
                {
                    DiffuseColor = Color.White.ToVector3(),
                    Direction = direction,
                    Enabled = true,
                    SpecularColor = Color.Red.ToVector3()
                },
                //AmbientLightColor = new Vector3(0.5f, 0, 0),
                EmissiveColor = new Vector3(0.5f, 0.5f, 0.5f)
            };
            //_effect.EnableDefaultLighting();
        }

        private void InitVertices()
        {
            _vertices = new VertexPositionColorNormal[TerrainWidth * TerrainHeight];

            var counter = 0;
            for (var y = 0; y < TerrainHeight; y++)
            {
                for (var x = 0; x < TerrainWidth; x++)
                {
                    _vertices[counter].Color = Heights.ColorAt(x, y);
                    _vertices[counter++].Position =
                        new Vector3(x * TriangleWidth, y * TriangleWidth, _heightData[x, y] / 3);
                }
            }
        }

        private void InitIndices()
        {
            _indices = new int[(TerrainWidth - 1) * (TerrainHeight - 1) * 6];
            var counter = 0;
            for (var y = 0; y < TerrainHeight - 1; y++)
            {
                for (var x = 0; x < TerrainWidth - 1; x++)
                {
                    var lowerLeft = x + y * TerrainWidth;
                    var lowerRight = x + 1 + y * TerrainWidth;
                    var topLeft = x + (y + 1) * TerrainWidth;
                    var topRight = x + 1 + (y + 1) * TerrainWidth;

                    _indices[counter++] = topLeft;
                    _indices[counter++] = lowerRight;
                    _indices[counter++] = lowerLeft;

                    _indices[counter++] = topLeft;
                    _indices[counter++] = topRight;
                    _indices[counter++] = lowerRight;
                }
            }

            Context.DebugStrings.Add($"Number of triangles: {_indices.Length / 3}");
        }

        private void AddNormals()
        {
            for (var i = 0; i < _indices.Length; i++)
            {
                var firstIndex = i++;
                var secondIndex = i++;
                var thirdIndex = i;

                var firstVertex = _vertices[_indices[firstIndex]];
                var secondVertex = _vertices[_indices[secondIndex]];
                var thirdVertex = _vertices[_indices[thirdIndex]];

                var sideA = firstVertex.Position - secondVertex.Position;
                var sideB = firstVertex.Position - thirdVertex.Position;

                var normal = Vector3.Cross(sideA, sideB);

                //+= because vertices belong to multiple triangles
                firstVertex.Normal += normal;
                secondVertex.Normal += normal;
                thirdVertex.Normal += normal;

                _vertices[_indices[firstIndex]] = firstVertex;
                _vertices[_indices[secondIndex]] = secondVertex;
                _vertices[_indices[thirdIndex]] = thirdVertex;
            }

            foreach (var vertex in _vertices)
            {
                vertex.Normal.Normalize();
            }
        }

        public void Draw(Matrix view, Matrix projection)
        {
            _effect.View = view;
            _effect.Projection = projection;

            foreach (var pass in _effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                _graphics.DrawUserIndexedPrimitives(PrimitiveType.TriangleList, _vertices, 0, _vertices.Length,
                    _indices, 0, _indices.Length / 3, VertexPositionColorNormal.VertexDeclaration);
            }
        }
    }
}