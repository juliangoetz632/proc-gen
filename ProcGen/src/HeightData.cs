﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ProcGen
{
    public interface IHeightData
    {
        void Load(ContentManager content);
        Color ColorAt(int x, int y);
        int TerrainWidth { get; set; }
        int TerrainHeight { get; set; }
        float[,] HeightData { get; set; }
        void ExportAsPng(GraphicsDevice graphics, string fileName);
        void Move();
    }
}