﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ProcGen
{
    public class Camera3D
    {
        private readonly GraphicsDevice _graphicsDevice;

        public float AngleSpeed = 2;
        public float HMovementSpeed = 50;
        public float VMovementSpeed = 30;
        public float PitchSpeed = 1f;

        public Vector3 Position = new Vector3(0, 0, 140);
        public float Angle;
        public float Pitch = -.5f;

        public float FieldOfView = MathHelper.PiOver4;
        public int NearPlaneDistance = 1;
        public int FarPlaneDistance = 10000;

        public Matrix ViewMatrix
        {
            get
            {
                var lookAtVector = new Vector3(0, 1, Pitch);
                var rotationMatrix = Matrix.CreateRotationZ(Angle);

                lookAtVector = Vector3.Transform(lookAtVector, rotationMatrix);
                lookAtVector += Position;

                var upVector = Vector3.UnitZ;

                return Matrix.CreateLookAt(
                    Position, lookAtVector, upVector);
            }
        }

        public Matrix ProjectionMatrix
        {
            get
            {
                var aspectRatio = _graphicsDevice.Viewport.Width / (float) _graphicsDevice.Viewport.Height;

                return Matrix.CreatePerspectiveFieldOfView(
                    FieldOfView, aspectRatio, NearPlaneDistance, FarPlaneDistance);
            }
        }

        public Camera3D(GraphicsDevice graphicsDevice)
        {
            _graphicsDevice = graphicsDevice;
            Context.KeyboardListener.KeyPressed += (sender, args) => { };
        }

        public void Update(GameTime gameTime)
        {
            var keyboardState = Keyboard.GetState();
            var delta = (float) gameTime.ElapsedGameTime.TotalSeconds;

            if (keyboardState.IsKeyDown(Keys.Left))
            {
                Angle += AngleSpeed * delta;
            }
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                Angle -= AngleSpeed * delta;
            }
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                var forwardVector = new Vector3(0, 1, 0);
                var rotationMatrix = Matrix.CreateRotationZ(Angle);
                forwardVector = Vector3.Transform(forwardVector, rotationMatrix);

                Position += forwardVector * HMovementSpeed *
                            delta;
            }
            if (keyboardState.IsKeyDown(Keys.Down))
            {
                var forwardVector = new Vector3(0, 1, 0);
                var rotationMatrix = Matrix.CreateRotationZ(Angle);
                forwardVector = Vector3.Transform(forwardVector, rotationMatrix);

                Position -= forwardVector * HMovementSpeed *
                            delta;
            }

            if (keyboardState.IsKeyDown(Keys.E))
            {
                var upwardVector = new Vector3(0, 0, 1);
                var rotationMatrix = Matrix.CreateRotationZ(Angle);
                upwardVector = Vector3.Transform(upwardVector, rotationMatrix);

                Position += upwardVector * VMovementSpeed *
                            delta;
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                var upwardVector = new Vector3(0, 0, 1);
                var rotationMatrix = Matrix.CreateRotationZ(Angle);
                upwardVector = Vector3.Transform(upwardVector, rotationMatrix);

                Position -= upwardVector * VMovementSpeed *
                            delta;
            }

            if (keyboardState.IsKeyDown(Keys.S))
            {
                Debug.WriteLine("Camera Pos: " + Position + " - Angle: " + Angle + " - Pitch: " + Pitch);
            }
            if (keyboardState.IsKeyDown(Keys.R))
            {
                Pitch += PitchSpeed * delta;
            }
            if (keyboardState.IsKeyDown(Keys.F))
            {
                Pitch -= PitchSpeed * delta;
            }
        }

        public void Initialize()
        {
        }
    }
}