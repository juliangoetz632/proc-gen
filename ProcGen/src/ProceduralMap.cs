﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace ProcGen
{
    public class ProceduralMap : IHeightData
    {
        public int TerrainWidth { get; set; }
        public int TerrainHeight { get; set; }
        public float[,] HeightData { get; set; }

        private float yOffset = 0f;

        public void Load(ContentManager content)
        {
            HeightData = new float[TerrainWidth, TerrainHeight];

            CalcHeights();
        }

        private void CalcHeights()
        {
            
            var perlinNoise = new PerlinNoise();
            for (var i = 0; i < TerrainWidth; i++)
            {
                var xoffset = 0f;
                for (var j = 0; j < TerrainHeight; j++)
                {
                    HeightData[i, j] = (float) perlinNoise.ValueAt(xoffset, yOffset, 0) * 255;
                    xoffset += 0.01f;
                }
                yOffset += 0.01f;
            }
        }

        public Color ColorAt(int x, int y)
        {
            var i = HeightData[x, y] / 255f;
            return new Color(i, i, i);
        }

        public void ExportAsPng(GraphicsDevice graphics, string fileName)
        {
            var generatedTexture = new Texture2D(graphics, TerrainWidth, TerrainHeight);

            var tmp = new float[HeightData.GetLength(0) * HeightData.GetLength(1)];
            Buffer.BlockCopy(HeightData, 0, tmp, 0, tmp.Length * sizeof(float));

            var colours = tmp.Select(i => i / 255).Select(i => new Color(i, i, i)).ToArray();

            generatedTexture.SetData(colours);

            Stream s = new FileStream(fileName, FileMode.Create);

            generatedTexture.SaveAsPng(s, TerrainWidth, TerrainHeight);
        }

        public void Move()
        {
            CalcHeights();
        }
    }
}